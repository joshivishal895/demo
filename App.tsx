
import React from 'react';

import {
  Text,
  View,
} from 'react-native';
import Todo from './src/Todo';
import { Provider } from 'mobx-react';
import TodosStore from './src/store/Todos';

// if(__DEV__) {
//   import('./ReactotronConfig').then(() => console.log('Reactotron Configured'))
// }
// import Reactotron from 'reactotron-react-native'

const App: React.FC = () => {

  return (
    <Provider store={new TodosStore()} >
      <View style={{ flex: 1 }}>
        <Todo />
      </View>
    </Provider>
  );
};

export default App;
