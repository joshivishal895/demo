import React from 'react';

import {
    View,
} from 'react-native';
import Item from './Item';
import { inject, observer } from "mobx-react";
import TodosStore from './store/Todos';
import { DataTemplate, commonProps } from './datatypes';

interface Props extends commonProps {
    store?: TodosStore
}

const LoadItems: React.FC<Props> = (props) => {

    return (
        <View style={{ flex: 1, paddingTop: 20 }}>
            {
                props.store?.todoList.map((element: DataTemplate) => {
                    return (
                        <Item
                            key={element.id?.toString()}
                            id={element.id}
                            name={element.name} />
                    )
                })
            }
        </View>
    )

}

export default inject("store")(observer(LoadItems));