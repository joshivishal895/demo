export interface DataTemplate {
    id: number | undefined,
    name: string
}

export interface commonProps {
    todoList?: Array<DataTemplate>
}