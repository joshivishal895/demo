import { observable, action } from "mobx";
import { DataTemplate } from '../datatypes';

class TodosStore {
    @observable todoList: DataTemplate[] = [];

    constructor() {
        this.todoList = [
            { id: 1, name: 'one' },
            { id: 2, name: 'two' },
            { id: 3, name: 'three' }];
    }

    @action addItem = (value: DataTemplate) => {
        this.todoList.push(value);
    }

    @action deleteItem = (id: number | undefined) => {
        console.log("id", id)
        this.todoList = this.todoList.filter(element => element.id !== id);
    }

    @action editItem = (value: DataTemplate) => {
        let matchIndex = this.todoList.findIndex(element => element.id === value.id);
        this.todoList[matchIndex] = value;
    }

   get todoItmes() {
        return this.todoList;
    }
}

export default TodosStore