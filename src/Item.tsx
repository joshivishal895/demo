import React, { useState } from 'react';

import {
    Text,
    View,
    TouchableOpacity
} from 'react-native';
import AddNewItemModal from './AddNewItemModal';
import { DataTemplate, commonProps } from './datatypes';
import { inject, observer } from "mobx-react";
import TodosStore from './store/Todos';

interface Props extends DataTemplate, commonProps {
    store?: TodosStore
}

const Item: React.FC<Props> = ({ id, name, store }) => {

    const [itemModalVisibility, setItemModalVisibility] = React.useState(false)

    return (
        <View key={id?.toString()} style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', backgroundColor: '#007755', marginBottom: 10, padding: 20 }}>
            <AddNewItemModal
                addNewItemModalVisibility={itemModalVisibility}
                setaddNewItemModalVisibility={setItemModalVisibility}
                editMode={true}
                initialValue={name}
                id={id} />

            <Text style={{ textAlign: 'center', color: 'white', fontSize: 18 }}>
                {name}
            </Text>
            <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity onPress={() => setItemModalVisibility(!itemModalVisibility) } style={{ padding: 5, width: 80, borderWidth: 1, backgroundColor: '#122312', marginRight: 10 }}>
                    <Text style={{ color: 'white', textAlign: 'center' }}>Edit</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => store?.deleteItem(id) } style={{ padding: 5, width: 80, borderWidth: 1, backgroundColor: 'white', borderColor: 'red' }}>
                    <Text style={{ color: 'red', textAlign: 'center' }}>Delete</Text>
                </TouchableOpacity>
            </View>
        </View>
    )

}

export default inject("store")(observer(Item))