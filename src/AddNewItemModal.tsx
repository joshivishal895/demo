import React from 'react';

import {
    Text,
    View,
    TouchableOpacity,
    Modal,
    TextInput
} from 'react-native';
import { inject, observer } from "mobx-react";
import TodosStore from './store/Todos';

interface Props {
    addNewItemModalVisibility: boolean
    setaddNewItemModalVisibility: (value: React.SetStateAction<boolean>) => void
    editMode: boolean
    initialValue?: string
    id?: number
    store?: TodosStore
}

const AddNewItemModal: React.FC<Props> = (props) => {

    const [newItemValue, setNewItemValue] = React.useState('');

    const editItem = () => {
        let value = { id: props.id, name: newItemValue }
        props.store?.editItem(value)
        props.setaddNewItemModalVisibility(!props.addNewItemModalVisibility)
    }

    return (
        <Modal
            animationType="slide"
            visible={props.addNewItemModalVisibility}
            onRequestClose={() => {
                props.setaddNewItemModalVisibility(!props.addNewItemModalVisibility);
            }}
        >
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <TextInput
                    style={{ height: 40, width: 300, margin: 12, borderRadius: 12, borderWidth: 1, padding: 10 }}
                    onChangeText={(value) => setNewItemValue(value)}
                    value={props.editMode ? props.initialValue : newItemValue}
                    placeholder="Enter New Value"
                />
                {
                    props.editMode ? <TouchableOpacity
                        style={{ width: 200, height: 40, backgroundColor: '#124589', justifyContent: 'center', marginBottom: 20 }}
                        onPress={() => editItem()}>
                        <Text style={{ fontSize: 14, textAlign: 'center', color: 'white' }}>Update</Text>
                    </TouchableOpacity> : <TouchableOpacity
                        style={{ width: 200, height: 40, backgroundColor: '#124589', justifyContent: 'center', marginBottom: 20 }}
                        onPress={() => {
                            props.store?.addItem({ id: props.store?.todoList.length + 1, name: newItemValue.toString() })
                            props.setaddNewItemModalVisibility(!props.addNewItemModalVisibility)
                        }}>
                            <Text style={{ fontSize: 14, textAlign: 'center', color: 'white' }}>Add</Text>
                        </TouchableOpacity>
                }
                <TouchableOpacity style={{ width: 200, height: 40, backgroundColor: 'red', justifyContent: 'center' }} onPress={() => props.setaddNewItemModalVisibility(!props.addNewItemModalVisibility)}>
                    <Text style={{ fontSize: 14, textAlign: 'center', color: 'white' }}>Close</Text>
                </TouchableOpacity>
            </View>
        </Modal>
    )

}

export default inject("store")(observer(AddNewItemModal));