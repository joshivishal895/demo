import React, { useState } from 'react';

import {
    Text,
    View,
    TouchableOpacity
} from 'react-native';
import LoadItems from './LoadItems';
import AddNewItemModal from './AddNewItemModal';
import { DataTemplate } from './datatypes';
import { inject, observer } from "mobx-react";

const sampleData: Array<DataTemplate> = [
    { id: 1, name: 'one' },
    { id: 2, name: 'two' },
    { id: 3, name: 'three' }
]

const Todo: React.FC<any> = (props) => {

    const [data, setData] = React.useState(props.store.todoList)
    const [addNewItemModalVisibility, setaddNewItemModalVisibility] = React.useState(false)

    return (
        <View style={{ flex: 1 }}>
            {/* Modal for adding data */}
            <AddNewItemModal
                addNewItemModalVisibility={addNewItemModalVisibility}
                setaddNewItemModalVisibility={setaddNewItemModalVisibility}
                editMode={false} />
            
            {/* Header */}
            <View style={{ height: 50, backgroundColor: '#225678', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: 'white', fontSize: 18 }}>Todo App with Typescript + Mobx</Text>
            </View>

            {/* Render Todo Items */}
            <LoadItems />

            {/* Add Item Button */}
            <TouchableOpacity style={{ position: 'absolute', right: 20, bottom: 40 }} onPress={() => {
                setaddNewItemModalVisibility(true)
            }}>
                <View style={{ width: 60, height: 60, backgroundColor: 'green', alignItems: 'center', justifyContent: 'center', borderRadius: 30 }}>
                    <Text style={{ fontSize: 30, color: 'white' }}>+</Text>
                </View>
            </TouchableOpacity>

        </View>
    )
};

export default inject("store")(observer(Todo));